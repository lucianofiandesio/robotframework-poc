*** Settings ***
Library     REST  localhost:8080
Library     Collections
Test Setup        Authenticate

# Resource    ${CURDIR}/imports/api_imports.robot
*** Variables ***
${kc_realm}=                  api-poc
${auth_server}=               http://localhost:9900/realms/${kc_realm}/protocol/openid-connect/token
${user}=                      john
${password}=                  password
${client_secret}=             T2Q7nvSlFaGmrxc3lpModr7r7OAEHk44
&{oauth2}=                    client_id=apipoc-client  username=${user}  password=${password}  grant_type=password  client_secret=${client_secret}
${oauth2_content_type}=       { "Content-Type": "aapplication/x-www-form-urlencoded" } 

*** Keywords ***
Authenticate
    POST                        ${auth_server}  headers={ "Content-Type": "application/x-www-form-urlencoded" }   data=${oauth2}  
    ${token}=                   String    response body access_token
    Set headers    { "Authorization": "Bearer ${token}[0]" }
    

*** Test cases ***
# https://jjtheengineer.blogspot.com/2018/05/restinstance-how-to-set-headers.html
    
Get Test Mandate
    GET             /0.1/mandates/test
    Integer        response status    200