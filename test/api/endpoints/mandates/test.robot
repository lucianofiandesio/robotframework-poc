*** Settings ***
Resource      ../../../../testdata/variables/imports.resource
Resource      ../auth.resource

Library     REST  ${GLOBAL_API_URL}

# similar to Junit `setUp`
#Test Setup        Authenticate

*** Test cases ***
    
Get Test Mandate
    Auth
    GET             /0.1/mandates/test
    Integer        response status    200
